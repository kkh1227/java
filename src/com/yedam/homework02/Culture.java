package com.yedam.homework02;

public abstract class Culture {

//	- 제목, 참여감독 수, 참여배우 수, 관객수, 총점을 필드로 가진다.
//	- 제목, 참여감독 수, 참여배우 수는 생성자를 이용하여 값을 초기화한다.
//	- 메소드는 다음과 같이 정의한다.
//	(1) public void setTotalScore(int score) : 관객수와 총점을 누적시키는 기능
//	(2) public String getGrade() : 평점을 구하는 기능
//	(3) public abstract void getInformation() : 정보를 출력하는 추상메소드
	
	//  필드
	
	String title;
	int dir;
	int actor;
	int aud = 0;
	int total = 0;
	
	// 생성자
	public Culture(String title, int dir, int actor) {
		this.title = title;
		this.dir = dir;
		this.actor = actor;
	}
	
	// 메소드
	
	public void setTotalScore(int score) {
		
		// 관객수 1씩 중가
		this.aud++;
		//점수 누적
		this.total += score;
		
	}
	
	public String getGrade() {
		int avg = total / aud;
		
		String grade = null;
		
		switch (avg) {
		case 1:
			grade = "☆";
			break;
		case 2:
			grade = "☆☆";
			break;
		case 3:
			grade = "☆☆☆";
			break;
		case 4:
			grade = "☆☆☆☆";
			break;
		case 5:
			grade = "☆☆☆☆☆";
			break;
			
		}
		
		//2번 방법 반복문
//		for(int i = 0; i<avg; i++) {
//			grade += "☆";
//		}
		return grade;
		
	}
	
	public abstract void getInformation();
		
	
}
